import com.google.gson.Gson;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class Client extends Application {
    static private InputStream inputStream;
    static private OutputStream outputStream;

    ByteArrayOutputStream baos;

    MyQuery query = new MyQuery();
    Gson gson = new Gson();
    String json;
    String maskAnswer;
    String clientError;
    String serverError;
    static String socketPath = "src/main/resources/socket.json";

    Alert alert = new Alert(Alert.AlertType.ERROR);

    final Label labelWord = new Label();
    final Label labelDescription = new Label();
    final Label textAnswer = new Label();
    final Label labelMask = new Label("% - ноль или более символов\n_ - один символ");

    final TextField textWordCreate = new TextField();
    final TextField textDescCreate = new TextField();

    final Button buttonFind = new Button("Найти");
    final Button buttonCreate = new Button("Создать");
    final Button buttonChange = new Button("Изменить");
    final Button buttonDelete = new Button("Удалить");
    final Button buttonCreateOk = new Button("Сохранить");
    final Button buttonChangeOk = new Button("Сохранить");
    final Button buttonFindMask = new Button("Поиск по маске");
    final Button buttonCancel = new Button("Отмена");

    final HBox hBoxLabel = new HBox();
    final HBox hBoxButton = new HBox(50);

    final VBox vBox = new VBox();

    final Answer[] answer = new Answer[1];

    final TextField textFind = new TextField("Введите слово");

    final FlowPane flowPane = new FlowPane();

    Group group = new Group();

    final Scene scene = new Scene(group, 400, 250);

    final ScrollPane scrollTextAnswer = new ScrollPane();

    FileReader scReader = null;
    SocketConnection socketConnection;

    HashMap<String, String> answerMap = new HashMap<>();

    byte[] out;
    int check = 0;
    int i = 0;
    int inLength;

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            scReader = new FileReader(new File(socketPath));
            socketConnection = gson.fromJson(scReader, SocketConnection.class);
        } catch (FileNotFoundException e) {
            System.out.println(e);
            clientError = "Отсутствует файл socket соединения";
            showError(true);
        }

        labelWord.setMaxSize(100, 100);
        labelWord.setMinSize(100, 100);
        labelWord.setWrapText(true);
        labelWord.setAlignment(Pos.TOP_LEFT);

        labelDescription.setMaxSize(250, 100);
        labelDescription.setMinSize(250, 100);
        labelDescription.setWrapText(true);
        labelDescription.setAlignment(Pos.TOP_LEFT);

        textWordCreate.setMinWidth(90);

        textDescCreate.setMinWidth(250);

        textAnswer.setMaxWidth(350);
        textAnswer.setWrapText(true);

        vBox.getChildren().addAll(hBoxLabel, hBoxButton);

        flowPane.getChildren().addAll(textFind, buttonFind, buttonFindMask);

        BorderPane borderPane = new BorderPane();
        BorderPane.setMargin(flowPane, new Insets(25));
        BorderPane.setMargin(vBox, new Insets(0, 0, 0, 10));
        borderPane.setCenter(flowPane);
        borderPane.setBottom(vBox);

        group.getChildren().addAll(borderPane);

        scrollTextAnswer.setContent(textAnswer);
        scrollTextAnswer.setPrefViewportHeight(75);
        scrollTextAnswer.setPrefViewportWidth(350);
        scrollTextAnswer.setPannable(true);

        labelMask.setLayoutX(10);
        textAnswer.setLayoutX(10);

        //Команды выполнения
        EventHandler<ActionEvent> find = event -> {
            requestResponse("find", textFind.getText(), null);
            labelWord.setText(query.getWord());

            if (answerMap.size() == 0) {
                labelDescription.setText("Такого слова в словаре нет. Вы можете создать его.");
                hBoxLabel.getChildren().addAll(labelWord, labelDescription);
                hBoxButton.getChildren().addAll(buttonCreate);
            } else {
                for (final Map.Entry<String, String> entry : answerMap.entrySet()) {

                    labelDescription.setText("- " + entry.getValue());
                }
                hBoxLabel.getChildren().addAll(labelWord, labelDescription);
                hBoxButton.getChildren().addAll(buttonChange, buttonDelete);
            }
        };

        buttonFindMask.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                hBoxButton.getChildren().add(labelMask);
                requestResponse("findMask", textFind.getText(), null);

                if (answerMap.isEmpty()) {
                    textAnswer.setText("Слова, соответствующие маске, не найдены");
                } else {
                    maskAnswer = "";
                    for (final Map.Entry<String, String> entry : answerMap.entrySet()) {
                        maskAnswer += entry.getKey() + " - " + entry.getValue() + "\n";
                    }
                    textAnswer.setText(maskAnswer);
                }
                hBoxLabel.getChildren().add(scrollTextAnswer);
            }
        });

        buttonDelete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                requestResponse("delete", labelWord.getText(), null);
                for (final Map.Entry<String, String> entry : answerMap.entrySet()) {
                    labelDescription.setText(entry.getValue());
                }
            }
        });

        EventHandler<ActionEvent> createSave = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                changeSave("create");
            }
        };

        EventHandler<ActionEvent> changeSave = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                changeSave("change");
            }
        };

        buttonCreate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                change("create", "");

            }
        });

        buttonChange.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                change("change", labelDescription.getText());

            }
        });

        buttonCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                hBoxButton.getChildren().clear();
                hBoxLabel.getChildren().clear();
            }
        });

        buttonCreateOk.setOnAction(createSave);
        buttonChangeOk.setOnAction(changeSave);

        textFind.setOnAction(find);
        buttonFind.setOnAction(find);

        primaryStage.setTitle("Толковый словарь");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void changeSave(String command) {

        requestResponse(command, labelWord.getText(),
                textDescCreate.getText());

        for (final Map.Entry<String, String> entry : answerMap.entrySet()) {
            labelWord.setText(entry.getKey());
            labelDescription.setText("- " + entry.getValue());
        }
        hBoxLabel.getChildren().addAll(labelWord, labelDescription);
    }

    private void change(String command, String firstDescription) {
        hBoxButton.getChildren().clear();
        hBoxLabel.getChildren().clear();

        query.setTypeQuery(command);

        String word = labelWord.getText();

        hBoxLabel.getChildren().clear();

        labelWord.setText(word);

        textDescCreate.setText(firstDescription);

        hBoxLabel.getChildren().addAll(labelWord, textDescCreate);

        switch (command) {

            case "create": {
                hBoxButton.getChildren().addAll(buttonCreateOk, buttonCancel);
                break;
            }
            case "change": {
                hBoxButton.getChildren().addAll(buttonChangeOk, buttonCancel);
                break;
            }
        }
    }

    private HashMap<String, String> requestResponse(String orderType, String word, String description) {

        hBoxButton.getChildren().clear();
        hBoxLabel.getChildren().clear();
        query.setTypeQuery(orderType);
        query.setWord(word);
        query.setDescription(description);

        baos = new ByteArrayOutputStream();

        try (Socket connection = new Socket(InetAddress.getByName(socketConnection.getName()), socketConnection.getPort())) {
            answerMap.clear();

            json = gson.toJson(query);

            outputStream = connection.getOutputStream();
            out = json.getBytes();
            outputStream.write(out);

            inputStream = connection.getInputStream();
            baos.write(inputStream.read());
            inLength = inputStream.available();

            for (int j = 0; j < inLength; j++) {
                i = inputStream.read();
                baos.write(i);
            }

            answer[0] = gson.fromJson((String) baos.toString(), Answer.class);
            baos.close();

            if (!answer[0].getError().equals("")){
                clientError = answer[0].getError();
                showError(false);
            }

            answerMap = answer[0].getWordMap();
        } catch (IOException e) {
            System.out.println(e);
            clientError = "Ошибка соединения: " + e;
            showError(false);
        } catch (RuntimeException e){
            System.out.println(e);
            clientError = "Ошибка инициализации объекта: " + e;
            showError(false);
        }
        return answerMap;
    }
    public void showError(boolean irreversibleError){
        alert.setTitle("Ошибка!");
        alert.setHeaderText("Описание:");
        alert.setContentText(clientError);
        alert.showAndWait();
        if (irreversibleError) {
            Platform.exit();
        }
    }
}